<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once('components/SBMService.php');
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Viza</title>
    <link rel="stylesheet" type="text/css" href="styles/all.min.css" />
    <link media="print, handheld" rel="stylesheet" href="styles/print.css">
    <link rel="stylesheet" type="text/css" href="styles/project.min.css" />
</head>
<body>
<?php
$vc = SBMService::getInstance()->getVisaCenter();
?>
<div class="content">

    <ul class="steps">
        <li class="approve active no_print" data-step="1">
            <h2><b>Согласие на обработку персональных данных</b></h2>
            <div class="step_content" style="display: block;">
                <div class="text">
                    <p>Я согласен с использованием моих личных данных, необходимых для записи на приём, а также подтверждаю
                        их достоверность и даю согласие на их дальнейшую обработку, передачу и хранение в целях
                        изготовления, оформления и контроля пакета документов в соответствии с требованиями Закона о защите
                        личных данных. С правилами пользования системой записи ознакомлен и несу ответственность за их
                        соблюдение.</p>
                </div>
                <a class="btn next">Согласен</a> <a class="btn">Не согласен</a>
            </div>
        </li>
        <li data-step="2" class="no_print">
            <span class="num">1</span>
            <h2>
                Выбор учреждения
                <small class="selected"></small>
            </h2>
            <div class="step_content">

                <div class="text">
                    При выборе учреждения предварительно убедитесь что оно обслуживает регион Вашего проживания.
                </div>

                <form>
                    <?php foreach($vc as $id => $title):?>
                        <p>
                            <input type="radio" value="<?php echo $id; ?>" name="center_id" id="in_<?php echo $id; ?>"/>
                            <label for="in_<?php echo $id; ?>"><?php echo $title; ?></label>
                        </p>
                    <?php endforeach; ?>
                </form>

                <a class="btn next">Далее</a>
            </div>
        </li>

        <li data-step="3" class="no_print">
            <div class="num">2</div>
            <h2>
                Правила пользования системой записи
            </h2>

            <div class="step_content">

                <div class="text">
                    Внимательно прочтите и строго следуйте правилам записи. Ответственность за их соблюдение ложится на
                    Вас. Записи, нарушающие правила, недействительны и удаляются автоматически без уведомления.
                    Претензии по этим записям не принимаются.

                    <div class="bordered">
                        <ol>
                            <li>
                                Каждый посетитель должен иметь собственную запись на прием с точным указанием вопроса,
                                по которому он обращается в выбранное учреждение.
                            </li>
                            <li>Одно время приема включает в себя обслуживание одного клиента по одному пакету
                                документов на одно физическое лицо.
                            </li>
                            <li>При записи необходимо указывать данные именно того лица, которое планирует подавать
                                документы. В случае если документы подаются представителем заявителя, указываются данные
                                лица, ходатайствующего о визе.
                            </li>
                        </ol>
                    </div>

                    <form>
                        <p><input type="checkbox" value="1" name="z1" id="z1"/> <label for="z1">Я внимательно и
                                полностью ознакомился с правилами и несу ответственность за ихсоблюдение</label></p>
                    </form>
                    <a class="btn next">Далее</a>
                </div>

            </div>
        </li>

        <li data-step="4" class="no_print">
            <div class="num">3</div>
            <h2>
                Выбор вопроса
                <small class="selected"></small>
            </h2>

            <div class="step_content">

                <div class="text">
                    <p>Будьте внимательны! Вы будете приняты только по тем вопросам, которые будут перечислены в Вашей
                        предварительной записи. В случае Вашей невнимательности, забывчивости или ошибки претензии не
                        принимаются.</p>

                    <p>Для продолжения регистрации записи на прием выберите соответствующий Вам вопрос:</p>

                    <form id="services">
                    </form>
                    <a class="btn next">Далее</a>
                </div>

            </div>
        </li>
        <li data-step="5" class="no_print">
            <div class="num">4</div>
            <h2>
                Выбор даты
                <small class="selected"></small>
            </h2>

            <div class="step_content">

                <div class="text">
                    <p>Выберите дату на календаре (открытые дни помечены зелёным), после выбора даты у Вас появится
                        возможность выбрать удобное для Вас время. Если Вас устраивают выбранные дата и время, нажмите
                        “Далее”.</p>

                    <form>
                        <input type="hidden" id="date" name="date" value=""/>
                        <div id="datepicker"></div>
                    </form>
                    <a class="btn next">Далее</a>
                </div>

            </div>
        </li>
        <li data-step="6" class="no_print">
            <div class="num">5</div>
            <h2>
                Выбор времени
                <small class="selected"></small>
            </h2>

            <div class="step_content">

                <div class="text">
                    <form>
                        <ul id="time">
                        </ul>
                    </form>
                    <a class="btn next">Далее</a>
                </div>

            </div>
        </li>
        <li data-step="7" class="no_print">
            <div class="num">6</div>
            <h2>
                Ввод личных данных
            </h2>

            <div class="step_content">

                <div class="text">
                    <p>Заполнив анкету, поставьте отметку “Я согласен с использованием ...” и нажмите "Записаться".</p>

                    <form id="reserve_form">
                        <div class="profile_form">
                            <table>
                                <tr>
                                    <td>Фамилия <sup>*</sup></td>
                                    <td>
                                        <input type="text" name="lastname"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Имя, Отчество <sup>*</sup></td>
                                    <td>
                                        <input type="text" name="firstname"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Дата рождения <sup>*</sup></td>
                                    <td>
                                        <input type="text" id="birth_date" name="birthdate"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Телефон для контакта (мобильный) <sup>*</sup></td>
                                    <td>
                                        <input type="text" name="phone"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Адрес электронной почты <sup>*</sup></td>
                                    <td>
                                        <input type="text" name="email"/>
                                    </td>
                                </tr>
                                <!--tr>
                                    <td>Введите текст с картинки <sup>*</sup></td>
                                    <td>
                                        <img style="width: 100px;" src="../img/capcha.png"/><br/>
                                        <input type="text"/>
                                    </td>
                                </tr-->
                            </table>
                            <input type="radio" name="prof_agree" id="prof_agree"> <label for="prof_agree">Я
                                согласен....</label>
                        </div>

                    </form>
                    <a class="btn next">Далее</a>
                </div>

            </div>
        </li>
        <li data-step="8" class="last" class="no_print">
            <div class="num no_print">7</div>
            <h2 class="no_print">Подтверждение записи</h2>

            <div class="step_content">
                <div class="text to_print">
                    <p>Ваша запись выполнена успешно!</p>
                    <p>Заполните или запишите "дату и время приема", а так же "идентификатор записи". Внимательно
                        прочтите и
                        строго следуйте правилам записи.
                        Ответственность за их соблюдение ложится на Вас. Записи, нарушающие правила,недействительны и
                        удаляются автоматически без уведомления. Претензии по этим записям не принимаются
                    </p>

                    <div class="text_padd to_print">

                    </div>

                    <div class="btn no_print js-print-btn">Распечатать</div>
                    <div class="btn no_print pdf-btn">Сохранить PDF</div>
                </div>
            </div>
        </li>
    </ul>

</div>

<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script type="text/javascript" src="js/all.min.js"></script>
</body>
</html>