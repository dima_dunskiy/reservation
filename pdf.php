<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once 'lib/dompdf/autoload.inc.php';

// reference the Dompdf namespace
use Dompdf\Dompdf;

// instantiate and use the dompdf class
$dompdf = new Dompdf();


$html = '<html><head><style>body { font-family: DejaVu Sans }</style></head>' .
    mb_convert_encoding($_GET['html'],'UTF-8') . '</html>';

$dompdf->loadHtml($html);

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('A4', 'landscape');

// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
$dompdf->stream('document');


?>