<?php

class SBMService {
    //CONST WSDL = 'http://78.110.48.99/gsoap/sbmappservices72.wsdl';
    //CONST LOCATION = 'http://78.110.48.99/gsoap/gsoap_ssl.dll?sbmappservices72';
    CONST WSDL = 'http://62.141.88.108:51221/gsoap/sbmappservices72.wsdl';
    CONST LOCATION = 'http://62.141.88.108:51221/gsoap/gsoap_ssl.dll?sbmappservices72';
    CONST LOGIN = 'api';
    CONST PASSWORD = 'api123';

    protected static $_instance;

    protected $client;

    private function __construct() {
        $this->client = new SoapClient(self::WSDL,
            array(
                'location' => self::LOCATION,
                'login' => self::LOGIN,
                'password' => self::PASSWORD
            ));
    }

    public static function getInstance() {
        if (self::$_instance === null) {
            self::$_instance = new self;
        }

        return self::$_instance;
    }

    private function __clone() {}

    public function getVisaCenter()
    {
        $result = $this->client->GetItemsByQuery(array(
            'auth' => array(
                'userId' => 'api',
                'password' => 'api123',
                'hostname' => '62.141.88.108',
                'port' => '51221'
            ),
            'table' => array(
                'dbName' => 'USR_CENTERS'
            ),
            'queryWhereClause' => "TS_COUNTRY=(select ts_id from usr_countries where ts_iso_code='LTU') AND TS_ACTIVEINACTIVE=0"
        ));

        $list = array();
        if ($result->return->totalCount == 1) {
            $list[$result->return->item->id->tableIdItemId] = $result->return->item->title;
        } elseif ($result->return->totalCount > 0) {
            foreach ($result->return->item as $item) {
                $list[$item->id->tableIdItemId] = $item->title;
            }
        }


        return $list;
    }

    public function getServices($id)
    {
        if (empty($id)) return array();

        $result = $this->client->GetItemsByQuery(array(
            'auth' => array(
                'userId' => 'api',
                'password' => 'api123',
                'hostname' => '62.141.88.108',
                'port' => '51221'
            ),
            'table' => array(
                'dbName' => 'USR_SERVICES_FOR_CENTER'
            ),
            'queryWhereClause' => "TS_CENTER=" . end(explode(':', $id))
        ));

        $list = array();
        if ($result->return->totalCount == 1) {
            $list[$result->return->item->id->tableIdItemId] = $result->return->item->title;
        } elseif ($result->return->totalCount > 0) {
            foreach ($result->return->item as $item) {
                $list[$item->id->tableIdItemId] = $item->title;
            }
        }

        return $list;
    }

    public function CreatePrimaryItem($params)
    {
        $data = array(
            'auth' => array(
                'userId' => 'api',
                'password' => 'api123',
                'hostname' => '62.141.88.108',
                'port' => '51221'
            ),
            'project' => array(
                'internalName' => 'USR_RESERVATIONS.LITHUANIA'
            ),
            'item' => array(
                'title' => $params['lastname'],
                'extendedField' => array(
                    array(
                        'id' => array(
                            'dbName' => 'CENTER'
                        ),
                        'value' => array(
                            'internalValue' => $params['center_id']
                        )
                    ),
                    array(
                        'id' => array(
                            'dbName' => 'SERVICE'
                        ),
                        'value' => array(
                            'internalValue' => $params['service_id']
                        )
                    ),
                    array(
                        'id' => array(
                            'dbName' => 'RESERVATION_DATE'
                        ),
                        'value' => array(
                            'internalValue' => $params['date']
                        )
                    ),
                    array(
                        'id' => array(
                            'dbName' => 'START_TIME'
                        ),
                        'value' => array(
                            'displayValue' => $params['time']
                        )
                    ),
                    array(
                        'id' => array(
                            'dbName' => 'NAME'
                        ),
                        'value' => array(
                            'internalValue' => $params['firstname']
                        )
                    ),
                    array(
                        'id' => array(
                            'dbName' => 'BIRTH_DATE'
                        ),
                        'value' => array(
                            'internalValue' => $params['birthdate']
                        )
                    ),
                    array(
                        'id' => array(
                            'dbName' => 'E_MAIL'
                        ),
                        'value' => array(
                            'internalValue' => $params['email']
                        )
                    ),
                    array(
                        'id' => array(
                            'dbName' => 'PHONE_NUMBER'
                        ),
                        'value' => array(
                            'internalValue' => $params['phone']
                        )
                    ),
                )
            ),
            'submitTransition' => array(
                'internalName' => 'RESERVATIONS.SUBMIT_FROM_UI'
            )
        );

        $result = $this->client->CreatePrimaryItem($data);

        return $result;
    }

}