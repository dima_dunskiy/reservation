<?php

class ReservationService {
    CONST WSDL = 'http://62.141.88.108:51221/res/Service1.svc?SingleWSDL';//http://78.110.48.99/res/Service1.svc?SingleWSDL
    CONST LOCATION = 'http://62.141.88.108:51221/res/Service1.svc';//'http://78.110.48.99/res/Service1.svc';
    CONST LOGIN = 'api';
    CONST PASSWORD = 'api123';

    protected static $_instance;

    protected $client;

    private function __construct() {
        $this->client = new SoapClient(self::WSDL,
            array(
                'location' => self::LOCATION,
                'login' => self::LOGIN,
                'password' => self::PASSWORD
            ));
    }

    public static function getInstance() {
        if (self::$_instance === null) {
            self::$_instance = new self;
        }

        return self::$_instance;
    }

    private function __clone() {}

    public function getFreeSlots($date, $service_id)
    {
        $result = $this->client->GetFreeSlots(array(
            'date' => $date,
            'serviceId' => end(explode(':',$service_id))
        ));

        $list = array();
        foreach ($result->GetFreeSlotsResult->string as $item) {
            $list[$item] = date('H:i', strtotime($item));
        }

        return $list;
    }

}