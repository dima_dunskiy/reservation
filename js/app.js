$(document).ready(function () {

    var step = 1;

    $('#myModal').modal();

    $("#datepicker").datepicker({
        dateFormat:'yy-mm-dd'
    });

    $('#birthdate').datepicker({
        dateFormat:'yy-mm-dd'
    });

    $(document).on('change', '#vc', function (e) {
        $("#service").empty();
        $("#time").html('');

        getServices($(this).val());
    });

    $(document).on('change', '#service', function (e) {
        $("#time").html('');
    });

    $(document).on('click', '#submit_but', function(e) {
        $.ajax({
            url: 'ajax.php',
            data: $('#reserve_form').serialize()+'&action=reserve',
            type: 'POST',
            dataType: 'json',
            success: function (data) {
                if (data.success == 1) {
                    $('.container').html('<h1>Забронировано</h1>');
                } else {
                    $('.container').html('<h1>Ошибка бронирования</h1>');
                }
            }
        });

    });

    function getFreeslots(date, service_id)
    {
        $("#time-slots").html('');
        $.ajax({
            url: 'ajax.php',
            data: {
                date: date,
                service_id: service_id,
                action: 'get-freeslots'
            },
            type: 'POST',
            dataType: 'json',
            success: function (data) {
                var i=0;
                $.each(data.freeslots, function(k, v) {
                    var ch = (i==0)?' checked="checked"':'';
                    $("#time").append(
                        '<li><input name="time" type="radio" value="'+k+'"'+ch+'>'+v+'</li>'
                    );
                    i=i+1;
                });
            }
        });
    }

    function getServices(vc_id) {
        $.ajax({
            url: 'ajax.php',
            data: {
                center_id: vc_id,
                action: 'get-service'
            },
            type: 'POST',
            dataType: 'json',
            success: function (data) {
                $.each(data.services, function(k, v) {
                    $("#service").append($('<option value="'+k+'">'+v+'</option>'));

                });
            }
        });
    }


    $(document).on('click', '#agree_but', function(e){
        $('#myModal').modal('hide');
        $('#main_div').toggle('slow');
    });

    $(document).on('click', '#next_but', function(e){
        if (step == 6) return false;

        if (step == 1) {
            if ($('#vc').val() == 0) return false;
            $('#prev_but').show();
        }
        if (step == 2) {
            if (!$('#is_read').prop("checked")) return false;
        }
        if (step == 3 && $('#service').val() == 0) return false;

        if (step == 4) {
            getFreeslots($("#datepicker" ).val(), $('#service').val());
        }

        if (step == 5) {
            $('#submit_but').show();
        }

        $('#step'+step).toggle('slow');
        step = step+1;
        $('#step'+step).toggle('slow');

        if (step == 6) $('#next_but').hide();
    });

    $(document).on('click', '#prev_but', function(e){
        if (step == 1) return false;
        if (step == 5) {
            $('#next_but').show();
            $('#submit_but').hide();
        }

        $('#step'+step).toggle('slow');
        step = step-1;
        $('#step'+step).toggle('slow');
    });

});