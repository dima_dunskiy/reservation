<?php

require_once('components/SBMService.php');
require_once('components/ReservationService.php');


if (!empty($_POST['action'])) {
    switch($_POST['action']) {
        case 'get-service':
            $services = (!empty($_POST['center_id'])) ? SBMService::getInstance()->getServices($_POST['center_id']) : array();
            echo json_encode(array('services' => $services));
            exit;
            break;

        case 'get-freeslots':
            $freeslots = (!empty($_POST['date']) && !empty($_POST['service_id'])) ? ReservationService::getInstance()->getFreeSlots($_POST['date'], $_POST['service_id']) : array();
            echo json_encode(array('freeslots' => $freeslots));
            exit;
            break;

        case 'reserve':
            $data = $_POST;
            $result = SBMService::getInstance()->CreatePrimaryItem($data);
            if (empty($result->return->item)) {
                echo json_encode(array('success' => 0, 'html' => 'Ошибка бронирования'));
                exit;
            }
            $item = $result->return->item;

            $res = array(
                'id' => $item->id->displayName,
                'lastname' => $item->title,
                'firstname' => $item->extendedField[6]->value->displayValue,
                'birthdate' => $item->extendedField[4]->value->displayValue,
                'phone' => $item->extendedField[7]->value->displayValue,
                'email' => $item->extendedField[5]->value->displayValue,
                'target' => $item->extendedField[9]->value->displayValue,
                'date' => $item->extendedField[1]->value->displayValue,
                'time' => $item->extendedField[2]->value->displayValue
            );
            $fl = setcookie("reservData", json_encode($res), 3600, '/', $_SERVER['HTTP_HOST'], false);

            $res_html = sprintf('<p><b>%s %s</b></p>
                        <p>Идентификатор записи: <b>%s</b></p>
                        <p>Дата рождения: <b>%s</b></p>
                        <p>Телефон: <b>%s</b></p>
                        <p>Адрес электронный почты: <b>%s</b></p>
                        <p>Цель посещения: <b>%s</b></p>
                        <p>Дата и время приема: <b>%s %s</b></p>',
                $res['lastname'],
                $res['firstname'],
                $res['id'],
                $res['birthdate'],
                $res['phone'],
                $res['email'],
                $res['target'],
                $res['date'],
                $res['time']
                );
            echo json_encode(
                (isset($item)) ?
                    array('item'=>$item,'html'=>$res_html,'success' => 1) :
                    array('success' => 0, 'html' => 'Ошибка бронирования')
            );
            exit;
            break;
    }
}
